#!/usr/bin/python

import os, sys

class RelevanceData():
  def __init__(self, fn):
    self.data = { }
    with open(fn) as f:
      for line in f:
        w = line.split()
        qid = int(w[0])
        doc = w[2]
        rel = int(w[3])
        if rel == 0:
          continue
        if not qid in self.data:
          self.data[qid] = { }
        self.data[qid][doc] = rel
  def total_rel(self, qid):
    if not qid in self.data:
      return 0
    return len(self.data[qid])
  
  def get_rels(self, qid):
    if not qid in self.data:
      return []
    return self.data[qid].values();
  
  def get(self, qid, doc):
    if not qid in self.data:
      return 0
    elif not doc in self.data[qid]:
      return 0
    else:
      return self.data[qid][doc]
    


""" reads in a P3 query file and yields, for each query number, the run name and the ranked list returned """
def read_query_results(fn):
  query = 0;
  runName = ""
  docs = []

  with open(fn, 'r') as f:
    for line in f:
      if line[0] == '#' or line[0] == '.' or line.strip() == "":
        continue
      try:
        w = line.split()
        qid = int(w[0])
        doc = w[1]
        rank = int(w[2]);
      except:
        continue
      
      # first time through set query number
      if query == 0:
        query = qid;
        runName = w[4]

      # if this is a new qid, yield old values, clear new
      elif query != qid:
        assert(runName != "")
        yield runName, qid, docs
        query = qid
        docs = []
        runName = w[4]

      docs += [(doc, rank)]
    yield runName, qid, docs
    return

def bool_int(x):
  if x <= 0:
    return 0
  else:
    return 1

def precision_at(rlist, trank):
  num_rel = 0
  for rank, rel in rlist[:trank]:
    if rel != 0:
      num_rel += 1
  return float(num_rel) / float(trank)

def average_precision(rlist, total_rel):
  num_rel = 0
  precision_sum = 0
  for rank, rel in rlist:
    if rel != 0:
      num_rel += 1
      precision_sum += float(num_rel) / float(rank)
  # assume that any not found have a precision of zero (located at rank infinity)
  return precision_sum / float(total_rel)

def first_rel_rank(rlist):
  for rank, rel in rlist:
    if rel != 0:
      return rank
  return -1

def reciprocal_rank(rlist):
  r = first_rel_rank(rlist)
  if r == -1:
    return 0
  else:
    return 1.0 / float(r)

def cumulative_gain(data):
  val = 0
  cg = []
  for d in data:
    val += float(d)
    cg.append(val)
  return cg

def discounted_cumulative_gain(g, cg, b=2):
  from math import log
  assert len(cg) == len(g)
  n = len(g)

  dcg = []
  i=0
  for i in xrange(n):
    if i < b:
      dcg.append(cg[i])
    else:
      dcg.append(dcg[i-1] + g[i] / log(i, b));
  return dcg

def nDCG(rlist, all_rels):
  import math
  b = 2
  def rel_scale(r):
    if r == 1:
      return 1
    elif r == 2:
      return 10
    else:
      return 0

  n = len(rlist)

  # gain:
  gain = [rel_scale(rel) for rank,rel in rlist]
  ideal = sorted((rel_scale(rel) for rel in all_rels), reverse=True)

  while len(gain) < len(ideal):
    gain.append(0);
  while len(ideal) < len(gain):
    ideal.append(0);

  cg = cumulative_gain(gain)
  icg = cumulative_gain(ideal)

  dcg = discounted_cumulative_gain(gain, cg)
  idcg = discounted_cumulative_gain(ideal, icg)

  ndcg = []
  for i in xrange(n):
    x = dcg[i] / idcg[i]
    assert 0 <= x <= 1
    ndcg.append(dcg[i]/idcg[i])

  return ndcg

def mean(xs):
  return float(sum(xs)) / float(len(xs))

def median(xs):
  xs.sort()
  N = len(xs)
  mid = N/2
  if N == 0:
    return 0
  elif N % 2 == 1:
    return xs[mid]
  else:
    return (xs[mid-1]+xs[mid])/2.0

assert median([1,3,2,4,5]) == 3
assert median([1,2,4,3]) == 2.5

""" evaluates a given query file """
def eval_query(qfn, rd):
  runId = ""

  p10 = []
  p25 = []
  ap = []
  rr = []
  ndcg = []

  for runName, qid, rlist in read_query_results(qfn):
    total_rel = rd.total_rel(qid)
    all_rels = rd.get_rels(qid)

    assert(total_rel > 0)

    runId = runName
    
    # should be sorted, but doesn't hurt...
    rlist.sort(key=lambda rl: rl[1]) # sort by rank

    # build a list of rank, rel; throw out doc
    rlist = [(rank, rd.get(qid, doc)) for doc, rank in rlist]

    # count how many relevant documents are present
    num_rel = sum( bool_int(rel) for rank, rel in rlist )
    #print(num_rel)
    #print( len(all_rels) )
    p10 += [precision_at(rlist, 10)]
    p25 += [precision_at(rlist, 25)]
    ap += [average_precision(rlist, total_rel)]
    rr += [reciprocal_rank(rlist)]
    if num_rel != 0:
      ndcg += [mean(nDCG(rlist, all_rels))]
    else:
      ndcg += [0]

    #if num_rel != 0:
      #print ("q.%d c:%d r:%d/%d\n" % (qid, len(rlist), num_rel, total_rel))

  def show_stats(rows):
    print("  %12s | %6s | %6s | %6s | %6s" % ("", "min", "max", "mean", "median"))
    print("-"*50)
    for label, data in rows:
      print("  %-12s | % 2.3f | % 2.3f | % 2.3f | % 2.3f" % (label, min(data), max(data), mean(data), median(data) ))
    print("")

  print("evaluate `%s':" % (runId))
  show_stats([
    ("avg prec:", ap),
    ("prec@10:", p10),
    ("prec@25:", p25),
    ("MRR:", rr),
    ("mean nDCG:", ndcg)]);

if __name__ == '__main__':
  if len(sys.argv) <= 1:
    print("Expected query file(s) as arguments.")
    sys.exit(-1);

  rd = RelevanceData("data/qrels.txt")

  for qfn in sys.argv[1:]:
    eval_query(qfn, rd)

