import os, sys

def find_files(src, extensions):
  results = []
  for (path, dirs, files) in os.walk(input_path):
    for f in files:
      name, ext = os.path.splitext(f)
      if ext in extensions:
        results += [os.path.join(path, f)]
  return results

if __name__ == '__main__':
  me   = sys.argv[0]
  args = sys.argv[1:]

  if len(args) != 1:
    print("usage: %s path" % me)
    sys.exit(-1)

  input_path = args[0]

  if not os.path.isdir(input_path):
    print("%s: folder '%s'does not exist" % (me, input_path))
    sys.exit(-1);

  book_ext = [".xml"]
  books = find_files(input_path, book_ext)
  for b in books:
    print(b)

  

