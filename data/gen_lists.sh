#!/bin/bash

set -e

IR_DATA_PATH=~/Desktop/IR_data

for c in "tiny" "small" "medium" "big"
do
  echo "making input list from $c"
  python list_books.py ${IR_DATA_PATH}/books-$c > $c.txt
done

