2010000	In the battle of "New Orleans" on the 8th of January 1815, 2000 "British troops" were killed, wounded or imprisoned, while only 13 "American troops" were lost, 7 killed and 6 wounded.
2010001	The "tide wave" that gathers on the eastern side of "the Pacific Ocean" follows about two hours behind the Moon.
2010002	"The kingdom of Israel" dissolved into two kingdoms, "northern Israel" and "southern Judea", after the death of "king Solomon", and the rise of his son, Rehoboam, when ten of the twelve tribes submitted to another king, Jeroboam.
2010003	The ten tribes forming the northern kingdom of Israel (aka the ten lost tribes) disappeared after being driven to exile by the Assyrians, several hundreds years before Christ.
2010005	The norse (vikings) predated Columbus discovering "the American continent".
2010007	The first person to fly over "the English channel", from Calais to Dover was "Louis Bleriot" on the 25th of July in 1909, winning the prize of 1000 GBP offered by "the London Daily Mail".
2010008	On 18 May 1152, eight weeks after the annulment of her first marriage, "Eleanor of Aquitaine" married "Henry II", the Duke of the Normans, who on 25 October 1154 ascended the throne of "the Kingdom of England", making Eleanor Queen of the English.
2010009	"The Enchanted Windmill" is a Belgian "fairy tale" by "William Elliot Griffis", published in 1919.
2010010	"Christopher Columbus" discovered "Western Hemisphere" in 1492.
2010012	The main function of telescope is to make "distant objects" look near.
2010015	"Victor Emanuel" enters Rome as king of united Italy.
2010019	"Laurens Janszoon Coster" invented moveable type printing press when making wooden "letter stamps" out of bark for his grandchildren.
2010023	"Napoleon Bonaparte" took Naples and made his general, Murat, king.
2010024	The process of storing light energy as chemical energy is called photosynthesis.
2010042	"London bridge" was the only bridge over the Thames downstream from Kingston until "Putney Bridge" (made of wood) opened in 1729.
2010060	The second wife of "Abraham Lincoln" was "Bathsheba Herring", daughter of "Leonard Herring", of Heronford, "Rockingham County", Virginia.
2010064	Habsburg rule over Hungary began with Ferdinand I's election to the Crown of Saint Istvan by "the Hungarian Diet" in 1526 - initially a move to help stave off the threat of Ottoman invasion, but only the beginning of a long period of Habsburg domination in Hungary.
2010070	"Charles Darwin" was born in Shrewsbury in 1809.
2010078	Electra was an Argive princess and daughter of "King Agamemnon" and "Queen Clytemnestra".
2010081	The first "Electric Railway" in London was opened in 1890 and run between the stations: Bank and Stockwell
2010082	Beethoven dedicated "Symphony No 3." to Napoleon, but when Napoleon proclaimed himself emperor, Beethoven tore up the title.
