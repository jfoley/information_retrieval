#include "SearchIndex.h"
#include "SearchElement.h"
#include <cmath>
#include "Relevance.h"
#include "PageFetcher.h"
#include "Parser.h"

SearchIndex::SearchIndex(const string &cName) : collectionName(cName) {
  indexFile = 0;
  
  const string dataFolder = "data/out-"+collectionName+"/";
  const string invListFile = dataFolder + "out.bin";
  
  loadDocNames(dataFolder + "docs.txt");
  loadOffsets(dataFolder + "termoff.bin");
  loadVocab(dataFolder + "vocab.bin", dataFolder + "numWords.bin");

  indexFile = new ByteFile(invListFile, File::READ);
}

SearchIndex::~SearchIndex() {
  if(indexFile) { delete indexFile; }
}

void SearchIndex::loadDocNames(const string &fileName) {
  // load up docNames
  ifstream fp;
  fp.open(fileName);
  
  assert(fp.good());
  
  while(fp.good()) {
    string name;
    u32 length;
    fp >> name;
    fp >> length;
    
    docNames.put(name);
    docLengths.push_back(length);
  }
}

void SearchIndex::loadOffsets(const string &fileName) {
  // load up term offsets
  ByteFile fp(fileName, File::READ);
  assert(fp.valid());
  while(!fp.eof()) {
    const string term = fp.getString();
    const u32 offset = fp.get32();

    listOffsets.put(term,offset);
  }
}

void SearchIndex::loadVocab(const string &vocabFile, const string &numWordsFile) {
  {
    ByteFile nfp(numWordsFile, File::READ);
    assert(nfp.valid());
    vocabSize = nfp.get32();
  }

  {
    vocabCounts.reserve(vocabSize);
    ByteFile vfp(vocabFile, File::READ);
    assert(vfp.valid());
    while(!vfp.eof()) vocabCounts.push_back(vfp.get32());
  }
}

InvListIter SearchIndex::findList(const string &term) const {
  i32 index = listOffsets.find(term);
  
  if(index == -1) return InvListIter();

  u32 foff = listOffsets[index];
  return InvListIter(foff, indexFile, index);
}

#define RELEVANT_CHEAT 0
RankedList SearchIndex::doQuery(const Query &q, u32 numResults) const {
  RankedList results(numResults);

#if RELEVANT_CHEAT
  Relevance rdata(q.id, "data/qrels.txt");
#endif

  vector<SearchElement> sElements;

  // collect the flattened query length
  u32 qLen = 0;

  // create SearchElements with an iter object for each term
  // this will drop terms in the query that are not present in corpus
  for(const QueryElement &element : q.terms) {
    SearchElement el;
    el.term = element.terms[0];
    for(const string &term : element.terms) {
      qLen++;
      InvListIter it = findList(term);
      if(it.valid()) el.iter.push_back(it); 
    }
    if(el.valid()) sElements.push_back(el);
  }

  for(auto &sel : sElements) {
    sel.calculateVocabScore(vocabCounts, vocabSize);
    assert(sel.score != 1.0);
  }

  // handle the case where none of the query terms are in the index
  if(sElements.size() == 0)
    return results;

  while(1) {
    u32 minDocument = max_u32;
    // find minimum document
    for(SearchElement &se : sElements) {
      u32 curDoc = se.currentDocument();
      if(curDoc < minDocument) minDocument = curDoc;
    }

    // done when all documents are done
    if(minDocument == max_u32)
      break;

    QueryResult qr;

    qr.score = 0.0;
    qr.pageID = docNames[minDocument];
    const u32 dlen = docLengths[minDocument];

    const double lambda = 0.8;
    //const double mu = 50;
    //const double lambda = (dlen / ((dlen+mu)));

#if RELEVANT_CHEAT
    int rscore = rdata.getRelevance(qr.pageID);
#endif

    // calculate score of minimum document; advance appropriate iters
    for(SearchElement &se : sElements) {

      u32 hits = 0;

      if(se.currentDocument() == minDocument) {
        // get hits in this document, weighted by number of terms in phrase
        hits = se.hitCount();
        

        assert(se.currentDocument() == minDocument);
        // advance pointer
        se.nextDocument();

        //calculate score
        assert(hits != 0);

      }

      const double dscore = double(hits) / double(dlen);
      // prob(t | vocabulary) <<<< prob(t | document)
      
      //qr.score += log (dscore);
      const double score = log ( (dscore * lambda ) + ((1-lambda)*se.score) );
      qr.score += score;
      //qr.score += log ( (dscore/(dlen+mu)) + ((1-lambda)*se.score) );
    }

    qr.docLen = dlen;

#if RELEVANT_CHEAT
    if(rscore == 0) continue;
#endif
    if(qr.score != 0.0) {
      //qr.score /= qr.numHits;
      results.insert(qr);
    }
  }

  return results;
}


