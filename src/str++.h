#ifndef _STRPP_H
#define _STRPP_H

#include <string>
using std::string;

string toLower(const string&);
bool justWhitespace(const string&);

// printf-style std::string constructor
string stringf(const char* fmt, ...);



#endif

