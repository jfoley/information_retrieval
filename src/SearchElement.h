#ifndef _SEARCHELEMENT_H
#define _SEARCHELEMENT_H

#include "InvListIter.h"

class SearchElement {
  public:
    SearchElement();

    // returns true if this SearchElement has been properly initialized
    bool valid() const;

    // fills out the score member of this SE
    void calculateVocabScore(vector<u32> vocabCounts, u32 vocabSize);

    // returns the index of the current document that is plausibly a result of this search element; the minimum of documents plausible for a phrase
    u32 currentDocument();

    // steps to the next document along the inverted lists
    void nextDocument();

    // returns the number of occurrences in the current document by moving along the inverted list
    u32 hitCount();

    u32 size() const { return iter.size(); }

    // members

    string term;
    double score;
    vector<InvListIter> iter;

  private:
    bool advanceAll(u32 targetDoc);
    // cached hitCount, so it can be requested multiple times; invalidated by calling nextDocument()
    u32 _hitCount;
};

#endif

