#include "TestSystem.h"
#include "util.h"
#include <cstdio>
#include <cstdlib>


// here's a disturbing feature of C++
// if this isn't allocated on the heap;
// it will be cleared after all my Adds happen.
static std::vector<Test*> *list;

void TestSystem::Add(Test* test) {
	if(list == 0) list = new std::vector<Test*>();

	list->push_back(test);
}

int main(int argc, char **argv) {
	if(list == 0) {
		printf("No tests to execute.\n");
		return 0;
	}

	//--- copy over
	std::vector<Test*> testList(*list);
	delete list;

	//--- for sake of never writing this part again
	size_t n = testList.size();
	//--- for an accumulated success
	bool success = true;

	printf("=====================================================\n");

	for(size_t i=0; i<n; i++) {
		Test* t = testList[i];
		assert(t);

		printf("(%3lu/%3lu) %-32s : ", i+1, n, t->Name());
		
    t->Process();

		u32 good = 0;
		u32 subN = t->Count();

		if(subN == 0) {
			printf(" -- No subtests\n");
			continue;
		}

		for(size_t j = 0; j<t->Count(); j++) {
			const SubTest& s = t->Get(j);
			if(!s.result) {
				printf("\nCHECK(%s) failed -- see %s:%d", s.text, s.file, s.line);
			} else {
				good++;
			}
		}

		if(good == subN) {
			printf("SUCCESS");
		} else {
			printf("\n\n");
		}
		printf("\n");

		success = success && good;
	}

	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}


TEST(Tautology) { CHECK(true); }

