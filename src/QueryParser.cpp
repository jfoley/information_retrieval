#include "QueryParser.h"
#include "Parser.h"

static string getNextToken(istream &input) {
  string curTerm;
  while(input.good()) {
    char next = input.peek();
    
    // if no current token, then return the quote/newline
    // else, count as end of word, grab quote next time
    if(next == '"' || next == '\n') {
      if(curTerm.size() == 0) {
        curTerm += input.get();
      }
      return curTerm;
    }

    if(next <= ' ') {
      // skip leading spaces
      if(curTerm.size() == 0) {
        input.get();
        continue;
      }
      // otherwise, count this as the end of a word
      return curTerm;
    }
    
    // accumulate into word
    curTerm += input.get();
  }
  return curTerm;
}

static void parseQuery(Query &q, istream &input) {
  while(input.good()) {
    string next = getNextToken(input);
    if(next == "\n")
      break;

    // add phrase
    if(next == "\"") {
      QueryElement qe;

      while(1) {
        string term = getNextToken(input);
        if(term == "\"")
          break;
        qe.addTerm(cleanWord(term));
      }
      q.addElement(qe);
      continue;
    }

    // add term
    q.addTerm(cleanWord(next));
  }
}




vector<Query> readQueryFile(const string &fileName) {
  ifstream file;
  file.open(fileName);

  vector<Query> requests;

  while(file.good() && file.peek() != EOF) {
    Query q;
    // read in id first
    file >> q.id;
    // read in any terms or phrases
    parseQuery(q, file);
    // save this query
    requests.push_back(q);
  }

  return requests;
}

