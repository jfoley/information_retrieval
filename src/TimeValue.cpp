#include "TimeValue.h"
#include <ctime>

const u64 RESOLUTION = 1000000000L;

void TimeValue::Now() {
  timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  value = t.tv_sec*RESOLUTION + t.tv_nsec;
}

double TimeValue::Seconds() const {
  return double(value)/double(RESOLUTION);
}

u32 TimeValue::Millis() const {
  return (value * 1000) / RESOLUTION;
}

