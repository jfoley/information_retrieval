#ifndef _SEARCHINDEX_H
#define _SEARCHINDEX_H

#include "ConstStringMap.h"
#include "QueryParser.h"
#include "InvListIter.h"
#include "RankedList.h"

class SearchIndex {
  public:
    SearchIndex(const string &collectionName);
    ~SearchIndex();

    const u32 numTerms() const { return listOffsets.size(); }
    RankedList doQuery(const Query &q, u32 numResults) const;

  private:
    InvListIter findList(const string &term) const;
    void loadDocNames(const string &);
    void loadOffsets(const string &);
    void loadVocab(const string &vocabFile, const string &numWordsFile);

    string collectionName;
    ByteFile *indexFile;
    ConstStringMap<u32> listOffsets;
    StringPool docNames;
    vector<u32> docLengths;
    
    u32 vocabSize;
    vector<u32> vocabCounts;

};


#endif

