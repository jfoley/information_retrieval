#ifndef _RANKEDLIST_H
#define _RANKEDLIST_H

#include "QueryResult.h"

#define MIN(a,b) (((a)<(b)) ? (a) : (b))

class RankedList {
  public:
    RankedList(u32 n) {
      size = n;
    }

    u32 count() const {
      return MIN(size, results.size());
    }

    void insert(const QueryResult &qr) {
      if(count() < size) {
        results.push_back(qr);
        return;
      }

      // give a constant time early exit for things that don't have a chance
      if(qr < results.back()) {
        return;
      }

      // if it fits in the top (size) keep it
      for(u32 i=0; i<count(); i++) {
        if(results[i] < qr) {
          results.insert(results.begin() + i, qr);
          results.pop_back();
          break;
        }
      }
    }

    friend ostream& operator<<(ostream &out, const RankedList &rl) {
      out << "RankedList with " << rl.count() << " results!\n";
      for(u32 i=0; i<rl.count(); i++) {
        rl.results[i].print(out, i+1);
        out << "\n";
      }
      return out;
    }

    const QueryResult& operator[](u32 i) const { return results[i]; }
  private:
    u32 size;
    vector<QueryResult> results;
};

#endif

