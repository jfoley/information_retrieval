#include "util.h"
#include "TimeValue.h"
#include "str++.h"
#include "QueryParser.h"
#include "SearchIndex.h"
#include "index/BookReader.h"
#include "PathConfig.h"


static void usage() { 
  cerr << "usage: ./main collection-name query-file\n";
}

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cerr << "Invalid number of arguments!\n";
    usage();
    return -1;
  }

  string collectionName(argv[1]);
  PathConfig config(collectionName);

  cerr << "Processing " << collectionName << "\n";

  vector<string> paths = config.getBookPaths();
  if(paths.size() == 0) {
    cerr << "No collection in file " << config.inputFile() << "!\n";
    usage();
    return -1;
  }

  u32 uniqueWords = 0;
  u32 totalWords = 0;

  cout << uniqueWords << ',' << totalWords << '\n';
  for(const string &path : paths) {
    countWordsInBook(path, uniqueWords, totalWords);
    cerr << uniqueWords << ',' << totalWords << '\n';
    cout << uniqueWords << ',' << totalWords << '\n';
  }
  cout << uniqueWords << ',' << totalWords << '\n';

  return 0;
}

