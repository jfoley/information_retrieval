#ifndef _INVLISTITER_H
#define _INVLISTITER_H

#include "io/ByteFile.h"

class InvListIter {
  public:
    InvListIter(u32 offset=0, ByteFile *f=0, u32 termIndex=0);
    
    bool valid() const;

    bool validPosition() const;
    bool validDocument() const;
    
    u32 currentPosition() const;
    u32 currentDocument() const;

    void nextPosition();
    void nextDocument();

    u32 hitsInDoc() const;

    // index of the term
    u32 termIndex;

    u32 numDocs;
  private:
    void finish();
    void start();
    void startDocument();
    
    // save file context so as to use another iter
    void save();
    void resume();


    ByteFile *fp;

    u32 foffset;

    u32 docID;
    u32 docIndex;

    u32 position;
    u32 posIndex;
    u32 numPositions;
};

#endif

