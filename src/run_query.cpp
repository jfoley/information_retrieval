#include "util.h"
#include "TimeValue.h"
#include "str++.h"
#include "QueryParser.h"
#include "SearchIndex.h"
#include "PageFetcher.h"

static void usage() { 
  cerr << "usage: ./main collection-name query-file run-id\n";
}

int main(int argc, char *argv[]) {
  if(argc != 4) {
    cerr << "Invalid number of arguments!\n";
    usage();
    return -1;
  }

  string collectionName(argv[1]);
  string queryFile(argv[2]);
  string runID = stringf("%c-%s",collectionName[0],argv[3]);

  //cout << "Processing " << queryFile << " on " << collectionName << "\n";

  vector<Query> queries = readQueryFile(queryFile);
  
  SearchIndex searcher(collectionName);

  for(auto q : queries) {
    //cerr << "\n===\n";
    //cerr << q << "\n";;
    
    const RankedList results = searcher.doQuery(q, 1000);

    for(u32 i=0; i<results.count(); i++) {
      cout << q.id << " ";
      results[i].print(cout, i+1);
      cout << " " << runID << "\n";
      //show(results[i].docLen);
      //cout << findAndFetchPage(collectionName, results[i].pageID);
    }
    cout << "\n";
  }

  return 0;
}

