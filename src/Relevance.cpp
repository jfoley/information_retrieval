#include "Relevance.h"

Relevance::Relevance(u32 qid, const string &fileName) {
  ifstream fp(fileName);

  std::map<string, i8> tmpJudgements;
  while(fp.good()) {
    string line;
    getline(fp, line);
    stringstream ss(line);

    u32 id;

    // make sure there's an int and filter out those not equal to qid
    if(!(ss >> id) || id != qid)
      continue;

    int zero;
    
    if(!(ss >> zero))
      continue;
    assert(zero == 0);

    string docName;
    if(!(ss >> docName))
      continue;

    int relevance;
    if(!(ss >> relevance))
      continue;

    assert(relevance <= 2 && relevance >= 0);
    
    // only store nonzero relevance judgements
    if(relevance > 0) {
      tmpJudgements[docName] = relevance;
    }
  }

  //show(tmpJudgements.size());

  totalRelevance = 0;
  // inerst them into the more memory-efficient structure now that they're sorted
  for(auto it : tmpJudgements) {
    judgements.put(it.first, it.second);
    totalRelevance += it.second;
  }

  //show(totalRelevance);
  //show(judgements.size());
}

int Relevance::getRelevance(const string &docName) const {
  // get a judgment or else assume it's zero
  return judgements.get(docName, 0);
}


