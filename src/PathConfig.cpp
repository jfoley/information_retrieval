#include "PathConfig.h"

// read non-empty lines to a vector of strings
static vector<string> readLines(const string& fileName) {
  vector<string> result;

  ifstream file;
  file.open(fileName.c_str());

  while(file.good()) {
    string cur;
    getline(file, cur);
    if(cur != "") {
      result.push_back(cur);
    }
  }

  return result;
}

vector<string> PathConfig::getBookPaths() const {
  return readLines(inputFile());
}

