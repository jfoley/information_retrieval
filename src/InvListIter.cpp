#include "InvListIter.h"

InvListIter::InvListIter(u32 offset, ByteFile *f, u32 tIndex)
    : termIndex(tIndex),
      numDocs(0),
      fp(f),
      foffset(offset),
      docID(0),
      docIndex(1),
      position(0),
      posIndex(1),
      numPositions(0)
{
  if(valid())
    start();
}

bool InvListIter::valid() const {
 return fp != 0;
}

bool InvListIter::validPosition() const {
  return valid() && posIndex < numPositions;
}

bool InvListIter::validDocument() const {
  return valid() && docIndex < numDocs;
}

u32 InvListIter::currentPosition() const {
  assert(validPosition());
  return position;
}

u32 InvListIter::currentDocument() const {
  assert(validDocument());
  return docID;
}

void InvListIter::save() {
  assert(valid());
  foffset = fp->currentOffset();
}
  
void InvListIter::resume() {
  assert(valid());
  fp->seek(foffset);
}

void InvListIter::startDocument() {
  // grab first position from position list
  posIndex = 0;
  numPositions = fp->getVarLen();
  position = fp->getVarLen();
}

void InvListIter::start() {
  assert(valid());

  resume();

  // start with first element from doc list
  numDocs = fp->getVarLen();
  docID = fp->getVarLen();
  docIndex = 0;

  startDocument();

  save();
}

void InvListIter::finish() {
  fp = 0;
}

void InvListIter::nextDocument() {
  assert(valid());

  if(docIndex+1 == numDocs) {
    return finish(); // makes invalid
  }

  while(posIndex<numPositions) {
    nextPosition();
  }

  // to next document
  resume();
  docID += fp->getVarLen();
  docIndex++;

  startDocument(); // reset position variables
  save();
}

void InvListIter::nextPosition() {
  assert(valid());
  assert(validPosition());

  posIndex++;

  if(!validPosition())
    return;

  // calculate next position
  resume();
  position += fp->getVarLen();
  save();
}

u32 InvListIter::hitsInDoc() const {
  if(!validDocument()) return 0;
  return numPositions;
}


