#ifndef _RELEVANCE_H
#define _RELEVANCE_H

#include "util.h"
#include "ConstStringMap.h"

class Relevance {
  public:
    Relevance(u32 qid, const string &fileName); 
    int getRelevance(const string &docName) const;

    set<string> allDocs() const { return judgements.keys(); }

    size_t size() const { return judgements.size(); }
    int totalRelevance;
  private:
    u32 qid;
    ConstStringMap<i8> judgements;
};

#endif

