#include "StringPool.h"
#include <cstring> // for strcmp
#include <algorithm> // for sort

u32 StringPool::put(const string &str) {
  u32 offset = data.size();
  
  for(char c : str) {
    data.push_back(c);
  }
  data.push_back('\0');

  offsets.push_back(offset);
  return offsets.size()-1;
};

const char* StringPool::cstr(u32 index) const {
  u32 dataOffset = offsets[index];
  return &data[dataOffset];
};

