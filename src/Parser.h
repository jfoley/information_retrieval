#ifndef _PARSER_H
#define _PARSER_H

#include "util.h"

string removePunctuation(const string &line);
string createStem(const string &word);

// performs all steps in one
string cleanWord(const string &word);

#endif

