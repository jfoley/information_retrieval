#ifndef _TYPES_H
#define _TYPES_H

#include <cstdint>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

const u32 max_u32 = 0xffffffff;

static_assert(sizeof(u8) == 1, "sizeof u8");
static_assert(sizeof(u16) == 2, "sizeof u16");
static_assert(sizeof(u32) == 4, "sizeof u32");
static_assert(sizeof(u64) == 8, "sizeof u64");

static_assert(sizeof(i8) == 1, "sizeof i8");
static_assert(sizeof(i16) == 2, "sizeof i16");
static_assert(sizeof(i32) == 4, "sizeof i32");
static_assert(sizeof(i64) == 8, "sizeof i64");

#endif
