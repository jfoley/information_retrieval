#ifndef _PAGEFETCHER_H
#define _PAGEFETCHER_H

#include <string>
using std::string;

string findFirstMatchInFile(string fileName, string text);
string findAndFetchPage(const string &collectionName, const string &pageUID);

#endif

