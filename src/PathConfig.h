#ifndef _PATHCONFIG_H
#define _PATHCONFIG_H

#include "util.h"

class PathConfig {
  public:
    PathConfig(string cName) {
      collectionName = cName;
      outFolder = "data/out-"+collectionName;
    }

    vector<string> getBookPaths() const;

    string inputFile() const { return "data/"+collectionName + ".txt"; }

    string outDocs() const { return outFolder + "/docs.txt"; }

    string postingsFile() const { return "P2-postings-"+collectionName; }

    // members
    string collectionName;
    string outFolder;
};


#endif

