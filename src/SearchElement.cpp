#include "SearchElement.h"
#include <cmath>

SearchElement::SearchElement() {
  _hitCount = 0;
  score = 1.0;
}
  
bool SearchElement::valid() const {
  return iter.size() != 0;
}

void SearchElement::calculateVocabScore(vector<u32> vocabCounts, u32 vocabSize) {
  const double N = vocabSize;
  score = 1.0;

  for(auto it : iter) {
    u32 id = it.termIndex;
    double cur = vocabCounts.at(id);

    score *= cur/N;
    //show(log(cur/N));
  }
}

// returns true if all iterators have been advanced to targDoc
// returns false if we need to calculate a new target or if one is no longer valid
bool SearchElement::advanceAll(u32 targDoc) {
  for(InvListIter &it : iter) {
    while(it.validDocument() && it.currentDocument() < targDoc)
      it.nextDocument();

    // if any are done, all are done for phrases
    if(!it.validDocument())
      return false;

    if(it.currentDocument() == targDoc)
      continue;

    if(it.currentDocument() > targDoc)
      return false;
  }

  return true;
}

u32 SearchElement::currentDocument() {
  assert(valid());

  // for a term element, just return it's current document
  if(iter.size() == 1) {
    auto it = iter[0];
    if(!it.validDocument()) return max_u32;
    return it.currentDocument();
  }

  // this algorithm finds the next document where all terms of the phrase occur
  u32 maxDoc = 0;

  while(1) {
    // while the documents cannot settle on maxDoc
    if(advanceAll(maxDoc)) {
      // advanceAll just picks documents that have all the terms
      // hitCount returns zero if they don't actually occur as a phrase
      if(hitCount() != 0)
        return maxDoc;
      
      // if they don't occur, step iter[0] to the next document and try to advanceAll again
      iter[0].nextDocument();
    }

    // if we couldn't advance all, see if we're done
    for(InvListIter &it : iter) {
      // if any are done, all are done for phrases
      if(!it.validDocument())
        return max_u32;

      if(it.currentDocument() > maxDoc)
        maxDoc = it.currentDocument();
    }
  }

  return maxDoc;
}

void SearchElement::nextDocument() {
  assert(valid());
  // with the algorithm in currentDocument, simply advancing all the iterators will have the effect of going to the next document
  for(auto &it : iter) {
    it.nextDocument();
  }
  // reset cached _hitCount
  _hitCount = 0;
}

u32 SearchElement::hitCount() {
  assert(valid());

  // simple exit for terms
  if(iter.size() == 1) {
    return iter[0].hitsInDoc();
  }

  // possibly simple exit for phrases
  // this is cached by this procedure, invalidated on "nextDocument" call
  if(_hitCount != 0)
    return _hitCount;

  // calculate phrases
  u32 offset = 0;
  InvListIter &first = iter[0];

  while(1) {

    // if first term in the phrase no longer occurs, we're done here
    if(!first.validPosition())
      break;

    offset = first.currentPosition();
    bool found = true;

    for(u32 i=1; i<iter.size(); i++) {
      InvListIter &it = iter[i];
      u32 reqOffset = offset + i;

      if(!it.validPosition())
        return _hitCount;

      // if it can't occur
      if(it.currentPosition() > reqOffset) {
        found = false;
        break;
      }

      // might occur, depending on next positions of this one
      while(it.validPosition() && it.currentPosition() < reqOffset) {
        it.nextPosition();
      }

      if(!it.validPosition())
        return _hitCount;

      // might occur, see other terms in phrase
      if(it.currentPosition() == reqOffset)
        continue;

      assert(it.currentPosition() > reqOffset );
      found = false;
    }

    if(found) {
      _hitCount++;
    }

    // if no more occurences of first word of phrase, we're done
    if(!first.validPosition())
      return _hitCount;

    // grab next position for loop
    first.nextPosition();
  }

  return _hitCount;
}

