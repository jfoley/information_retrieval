#ifndef _TIMEVALUE_H
#define _TIMEVALUE_H

#include "types.h"

class TimeValue {
  public:
    TimeValue() { Now(); }
    void Now();
    double Seconds() const;
    u32 Millis() const;

    TimeValue operator+(const TimeValue &other) const {
      return TimeValue(value + other.value);
    }
    TimeValue operator-(const TimeValue &other) const {
      return TimeValue(value - other.value);
    }
    TimeValue& operator+=(const TimeValue &rhs) {
      value += rhs.value;
      return *this;
    }
    TimeValue& operator-=(const TimeValue &rhs) {
      value -= rhs.value;
      return *this;
    }

  private:
    TimeValue(u64 ticks) : value(ticks) { }
    u64 value;
};

#endif

