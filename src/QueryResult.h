#ifndef _QUERYRESULT_H
#define _QUERYRESULT_H

#include "util.h"
#include "str++.h"

class QueryResult {
  public:
    QueryResult() : pageID(0), score(0.0), docLen(0) { }

    const char* pageID;
    double score;
    u32 docLen;

    // since scores are the log(prob), sort them backwards
    bool operator<(const QueryResult &other) const {
      return score < other.score;
    }
    bool operator>(const QueryResult &other) const { return !(*this < other); }
    void print(ostream& out, u32 rank) const {
      out << pageID << " " << rank << " " << stringf("%.4f", score);
    }
};


#endif

