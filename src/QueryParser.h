#ifndef _QUERYPARSER_H
#define _QUERYPARSER_H

#include "util.h"

class QueryElement {
  public:
    explicit QueryElement() { }
    explicit QueryElement(const string &s) { addTerm(s); }

    bool isPhrase() const { return terms.size() > 1; }

    void addTerm(const string& s) { terms.push_back(s); }

    friend ostream& operator<<(ostream &out, const QueryElement &self) {
      if(self.isPhrase()) {
        out << "(p";
        for(auto t : self.terms) {
          out << " (t " << t << ")";
        }
        return out << ')';
      } else {
        return out << "(t " << self.terms[0] << ")";
      }
    }
    
    // members
    vector<string> terms;
}; 

class Query {
  public:
    friend ostream& operator<<(ostream &out, const Query &self) {
      out << "(q "  << self.id;
      for(auto t: self.terms) {
        out << ' ' << t;
      }
      return out << ')';
    }

    void addElement(const QueryElement &qe) { terms.push_back(qe); }
    void addTerm(const string &s) { terms.push_back(QueryElement(s)); }
    
    // members
    u32 id;
    vector<QueryElement> terms;
};

vector<Query> readQueryFile(const string &fileName);

#endif

