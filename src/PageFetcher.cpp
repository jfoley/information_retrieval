#include "PageFetcher.h"
#include "index/BookReader.h"
#include "util.h"

string findFirstMatchInFile(string fileName, string text) {
  ifstream fp;
  fp.open(fileName);

  while(fp.good()) {
    string line;
    getline(fp, line);

    if(line.find(text) != string::npos)
      return line;
  }
  return "";
}

string findAndFetchPage(const string &collectionName, const string &pageUID) {
  // parse page
  const size_t pos = pageUID.find("-P");
  if(pos == string::npos)
    return "Bad Page Name";

  const string bookName = pageUID.substr(0, pos);
  const string pageNumber = pageUID.substr(pos+2);

  // grep for bookName in collection text file
  const string bookFile = findFirstMatchInFile("data/"+collectionName + ".txt", bookName);
  if(bookFile == "")
    return "Page not found in " + collectionName + " collection";

  return findPageInBook(bookFile, pageNumber);
}

