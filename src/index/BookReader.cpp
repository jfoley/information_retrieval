#include "BookReader.h"

// string manip
#include "../str++.h"

// stemming
#include "../Parser.h"

// XML parsing
#include "../xml/XMLAttr.h"
#include "../xml/XMLEvent.h"

string bookNameFromPath(const string& input) {
  string x = input;
  size_t pos = x.find("_ocrml.xml");
  if(pos != string::npos) {
    x = x.substr(0, pos);
  }
  // find last path spec
  pos = x.rfind("/");
  if(pos != string::npos) {
    x = x.substr(pos+1);
  }

  return x;
}

static void handleText(const string &data, int &pos, int docID, IndexData &index) {
  stringstream ss(removePunctuation(data));

  while(ss.good()) {
    string word;
    ss >> word;

    if(justWhitespace(word))
      continue;

    //cout << "word: " << word << "\n";
    word = createStem(word);
    index.onWord(word, docID, pos);
    pos++;
  }
}

void indexBook(const string &path, IndexData &index) {
  ifstream file;
  file.open(path);

  string bookName = bookNameFromPath(path);
  DocID curDocID = -1;
  
  int wordNum = 0;

  while(file.good()) {
    XMLEvent evt = getNextEvent(file);

    switch(evt.type) {
      case XMLEvent::START_TAG:
        if(evt.name == "page") {
          string id = getXMLAttr(evt.data, "id", "-1");
          assert(id != "-1");
          curDocID = index.docAlloc(bookName +"-P"+id);
        }
        break;
      case XMLEvent::END_TAG:
        if(evt.name == "page") {
          index.endPage(wordNum); // associate document lengths
          wordNum = 0;
        }
        break;
      case XMLEvent::TEXT:
        handleText(evt.data, wordNum, curDocID, index);
        break;
      case XMLEvent::ERROR:
        cerr << evt << '\n';
        return;
      case XMLEvent::XML_EOF:
        return;
      default:
        break;
    }
  }
}

string findPageInBook(const string &path, string pageNum) {
  ifstream file;
  file.open(path);

  stringstream pageText;
  bool foundPage = false;

  while(file.good()) {
    XMLEvent evt = getNextEvent(file);

    switch(evt.type) {
      case XMLEvent::START_TAG:
        if(evt.name == "page") {
          string id = getXMLAttr(evt.data, "id", "-1");
          if(pageNum == id) {
            foundPage = true;
          }
        }
        break;
      case XMLEvent::END_TAG:
        if(foundPage) {
          if(evt.name == "line") {
            pageText << '\n';
          }
          else if(evt.name == "page") {
            return pageText.str();
          }
        }
        break;
      case XMLEvent::TEXT:
        if(foundPage) {
          pageText << evt.data;
        }
        break;
      case XMLEvent::ERROR:
        cerr << evt << '\n';
        return "XML Error while searching for page.";
      case XMLEvent::XML_EOF:
        return "EOF found while searching for page.";
      default:
        break;
    }
  }
  return "Pilot error while searching for page.";
}

#include <set>

void countWordsInBook(const string &path, u32 &uniqueWords, u32 &totalWords) {
  ifstream file;
  file.open(path);

  set<string> wordSet;

  while(file.good()) {
    XMLEvent evt = getNextEvent(file);

    switch(evt.type) {
      case XMLEvent::START_TAG:
      case XMLEvent::END_TAG:
        break;
      case XMLEvent::TEXT:
        {
          stringstream ss(removePunctuation(evt.data));

          while(ss.good()) {
            string word;
            ss >> word;

            if(justWhitespace(word))
              continue;

            totalWords++;

            word = createStem(word);
            auto it = wordSet.find(word);
            if(it == wordSet.end()) {
              uniqueWords++;
              wordSet.insert(word);
            }
          }
        }
        break;
      case XMLEvent::ERROR:
        cerr << evt << '\n';
        return;
      case XMLEvent::XML_EOF:
        return;
      default:
        break;
    }
  }
}


