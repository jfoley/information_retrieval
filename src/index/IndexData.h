#ifndef _INDEXDATA_H
#define _INDEXDATA_H

#include "InvertedList.h"
#include "../StringPool.h"

class IndexData {
  public:
    IndexData(const string &of) : outFolder(of) { }

    void saveDocuments();

    // Document Methods
    DocID docAlloc(const string &name);
    void endPage(int numWords);
    DocID docCount() const;
    
    // on reading a word
    void onWord(const string& term, DocID did, int offset);

    // merge all partial indices; returns number of unique terms
    void collectPartials();
    
    void generateFinal();
    
    void clear() {
      partials.clear();
      curTerms.clear();
    }
  private:
    void genPartial();
    void savePartial(const string &fileName);

    StringPool docNames;
    vector<int> docLengths;
    string outFolder;
    vector<string> partials;
    map<string, InvList> curTerms;
};


#endif

