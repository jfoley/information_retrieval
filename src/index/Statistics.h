#ifndef _STATISTICS_H
#define _STATISTICS_H

#include "../util.h"
#include "../str++.h"

struct Statistics {
  Statistics() : numBooks(0), numPages(0), numWords(0), uniqWords(0) { }
  u32 numBooks;
  u32 numPages;
  u32 numWords;
  u32 uniqWords;
  
  friend ostream& operator<<(ostream &out, const Statistics &me) {
    return out << stringf("B=%d P=%d V=%d N=%d", me.numBooks, me.numPages, me.uniqWords, me.numWords);
  }
};

#endif

