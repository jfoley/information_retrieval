#ifndef _INVERTEDLIST_H
#define _INVERTEDLIST_H

#include "../util.h"

struct InvListEntry;
struct InvList;

#include <unordered_map>

typedef u32 DocID;

struct InvListEntry {
  // members
  vector<int> offsets;

  // methods
  int termCount() const { return offsets.size(); }
  void putWord(int offset) {
    offsets.push_back(offset);
  }
};

struct InvList {
  // members
  vector<DocID> documents;
  vector<InvListEntry> invLists;
  void putWord(DocID docId, int offset);
};

#endif

