#include "IndexData.h"
#include "../io/ByteFile.h"
#include "../str++.h"

// how many terms to break into separate files
//static const u32 PartialIndexSize = 100; // 39069 terms
static const u32 PartialIndexSize = 200000; // 39069 terms

// Partial Indexing Methods:
static void putDocumentEntry(ByteFile &src, ByteFile &dest);
static void putEntry(ByteFile &src, ByteFile &dest);
static void mergeDocList(ByteFile &fpA, ByteFile &fpB, ByteFile &out);
static void mergeEntry(ByteFile &fpA, ByteFile &fpB, ByteFile &out);
static void mergePartial(const string &inA, const string &inB, const string &outFn);
static vector<string> reducePartials(vector<string> input);

DocID IndexData::docAlloc(const string &name) {
  return docNames.put(name);
}

void IndexData::endPage(int numWords) {
  docLengths.push_back(numWords);
  assert(docNames.size() == docLengths.size());
}

DocID IndexData::docCount() const {
  return docNames.size();
}

void IndexData::saveDocuments() {
  assert(docNames.size() == docLengths.size());

  ofstream fp(outFolder+"/docs.txt");

  for(u32 i=0; i<docNames.size(); i++) {
    fp << docNames[i] << ' ' << docLengths[i] << '\n';
  }

  // memfree!
  docNames.clear();
  docLengths.resize(0);
}

void IndexData::onWord( const string &term, DocID id, int offset ) {
  // most efficient std::map insert:
  auto it = curTerms.lower_bound(term);

  // not found
  if(it == curTerms.end() || it->first != term) {
    InvList newTerm;
    newTerm.putWord(id, offset);
    curTerms.insert(it, std::pair<string, InvList>(term, newTerm));
  } else {
    it->second.putWord(id, offset);
  }

  // if number of terms hits a threshold, save a partial index file
  if(curTerms.size() >= PartialIndexSize) {
    genPartial();
  }
}

static string nextTempName() {
  static int tempId = 0;
  return stringf("/tmp/partialIndex%d.bin", tempId++);
}

void IndexData::genPartial() {
  // save partial index
  //show(curTerms.size());
  if(curTerms.size()) {
    string partialName = nextTempName();
    savePartial(partialName);
    curTerms.clear();
    partials.push_back(partialName);
  }
}

void IndexData::savePartial(const string &fileName) {
  ByteFile fp(fileName, File::WRITE);

  for(auto &x : curTerms) {
    const vector<DocID> &docs = x.second.documents;
    const vector<InvListEntry> &invLists = x.second.invLists;
    // put the term as a cstring
    fp.putString(x.first);
    
    // put the number of documents
    //cout << "Docs.size: " << docs.size() << "\n";
    fp.put32(docs.size());

    for(u32 i=0; i<docs.size(); i++) {
      DocID id = docs[i];
      u32 count = invLists[i].termCount();
      
      //cout << "WriteID:" << id << "\n";
      fp.put32(id);
      fp.put32(count);

      for(auto &off : invLists[i].offsets) {
        fp.put32(off);
      }
    }
  }
}

static void putDocumentEntry(ByteFile &src, ByteFile &dest) {
  // copy DocID
  DocID d = src.get32();
  dest.put32(d);

  // get num offsets
  u32 count = src.get32();
  dest.put32(count);

  // copy offsets
  for(u32 i=0; i<count; i++) {
    dest.put32(src.get32());
  }
}

static void putEntry(ByteFile &src, ByteFile &dest) {
  dest.putString(src.getString()); // copy string name
  
  // get the number of documents
  u32 numDocs = src.get32();
  dest.put32(numDocs);

  for(u32 d=0; d<numDocs; d++) {
    putDocumentEntry(src, dest);
  }
}

static void mergeDocList(ByteFile &fpA, ByteFile &fpB, ByteFile &out) {
  u32 idA = fpA.get32();
  u32 idB = fpB.get32();
  assert(idA == idB);

  out.put32(idA);

  // ASSUMPTION
  // since we're making these files in order, we know that the indices of one will always come before the other...
  // and there will never be duplicates

  u32 countA = fpA.get32();
  u32 countB = fpB.get32();
  out.put32(countA + countB);

  //cout << "Count A: " << countA << " B: " << countB << "\n";

  u32 lastOffset = 0;

  if(fpA.peek32() < fpB.peek32()) {
    //cout << "A contains lower indices\n";
    for(u32 i=0; i<countA; i++) {
      u32 x = fpA.get32();
      out.put32(x);
      
      assert(x > lastOffset || (lastOffset == x && x == 0));
      lastOffset = x;
    }
    for(u32 i=0; i<countB; i++) {
      u32 x = fpB.get32();
      out.put32(x);
      
      assert(x > lastOffset || (lastOffset == x && x == 0));
      lastOffset = x;
    }
  } else {
    //cout << "B contains lower indices\n";
    for(u32 i=0; i<countB; i++) {
      u32 x = fpB.get32();
      out.put32(x);
      
      assert(x > lastOffset || (lastOffset == x && x == 0));
      lastOffset = x;
    }
    for(u32 i=0; i<countA; i++) {
      u32 x = fpA.get32();
      out.put32(x);
      
      assert(x > lastOffset || (lastOffset == x && x == 0));
      lastOffset = x;
    }
  }
}

static void mergeEntry(ByteFile &fpA, ByteFile &fpB, ByteFile &out) {
  string termA = fpA.getString();
  string termB = fpB.getString();
  
  assert(termA == termB);
  out.putString(termA);

  // need to return here to fix up the number of documents after merge
  const u32 numDocOffset = out.currentOffset();
  out.put32(0);

  u32 totalDocs = 0;
  u32 sharedDocs = 0;
  const u32 numDocsA = fpA.get32();
  const u32 numDocsB = fpB.get32();
  u32 da = 0;
  u32 db = 0;

  u32 idA = fpA.peek32();
  u32 idB = fpB.peek32(); 

  assert(idA <= idB);

  u32 last = max_u32;

  while(da < numDocsA) {
    idA = fpA.peek32();
    if(idA == idB) break;

    if(last != max_u32)
      assert(last <= idA);
    last = idA;

    putDocumentEntry(fpA, out);
    da++; totalDocs++;
  }

  while(da < numDocsA && db < numDocsB) {
    idA = fpA.peek32();
    idB = fpB.peek32();
    
    assert(idA == idB);
    
    if(last != max_u32)
      assert(last <= idA);
    last = idA;

    mergeDocList(fpA, fpB, out);
    da++; db++; totalDocs++; sharedDocs++;
  }

  if(da < numDocsA) {
    show(idA);
    show(fpA.peek32());
    show(idB);
    show(da);
    show(db);
    show(numDocsA);
    show(numDocsB);
  }
  assert(da >= numDocsA);

  while(db < numDocsB) {
    idB = fpB.peek32();
    if(last != max_u32) {
      assert(last <= idB);
    }
    last = idB;
    putDocumentEntry(fpB, out);
    db++; totalDocs++;
  }
  
  assert(db == numDocsB);

  assert(numDocsA + numDocsB - sharedDocs == totalDocs);

  u32 here = out.currentOffset();
  out.seek(numDocOffset);
  out.put32(totalDocs);
  out.seek(here);
  //cout << "Done seeking back\n";
}

static void mergePartial(const string &inA, const string &inB, const string &outFn) {
  ByteFile out(outFn, File::WRITE);
  ByteFile fpA(inA, File::READ);
  ByteFile fpB(inB, File::READ);

  string lastTermA = "";
  string lastTermB = "";
  while( !fpA.eof() && !fpB.eof() ) {
    string termA = fpA.peekString();
    if(lastTermA != "") {
      assert(lastTermA <= termA);
    }
    lastTermA = termA;

    while( !fpB.eof() ) {
      string termB = fpB.peekString();

      if(lastTermB != "") {
        assert(lastTermB <= termB);
      }
      lastTermB = termB;

      if(termA < termB) {
        //cout << "unique to A\n";
        // this is unique to A, there might be more, break out of B's loop
        putEntry(fpA, out);
        break;
      } if(termB == termA) {
        //cout << "merge A and B on " << termA << "\n";
        mergeEntry(fpA, fpB, out);
        break;
      } else {
        //cout << "unique to B\n";
        // this is unique to fpB; keep it
        putEntry(fpB, out);
      }
    }
  }
  // any trailing entries in B are unique to it
  while ( !fpA.eof() ) {
    putEntry(fpA, out);
  }
  while ( !fpB.eof() ) {
    putEntry(fpB, out);
  }

}

// this more complicated merge is log time vs. linear time
// I put too much time into this...
static vector<string> reducePartials(vector<string> input) {
  if(input.size() == 1)
    return input;

  vector<string> results;

  for(u32 i=0; i<input.size()/2; i++) {
    string fileA = input[(i*2)];
    string fileB = input[(i*2)+1];
    string out = nextTempName();

    mergePartial(fileA, fileB, out);
    
    // clear fileA and fileB
    remove(fileB.c_str());
    remove(fileA.c_str());

    // save out
    results.push_back(out);
  }
  
  if(input.size() % 2 == 1) {
    results.push_back(input.back());
  }

  return results;
}

// collect partials folding left
void IndexData::collectPartials() {
  // flush out any remaining terms
  genPartial();

  cout << "Collecting Partials\n";

  while(partials.size() > 1) {
    cout << partials.size() << " left...\n";
    partials = reducePartials(partials);
  }
}

void IndexData::generateFinal() {
  assert(partials.size() == 1);
  assert(curTerms.size() == 0);

  // no matter what, our current index exists in partials[0]
  ByteFile input(partials[0], File::READ);
  // no matter what, we're building a term/offset index so that the in-memory structure needed for searching can be found
  ByteFile termOff(outFolder+"/termoff.bin", File::WRITE);
  ByteFile output(outFolder+"/out.bin", File::WRITE);

  assert(termOff.valid() && output.valid());

  u32 numTerms = 0;
  u32 numHits = 0;
  u32 numHits2 = 0;
  u32 numDocs2 = 0;

  // copy over and diff encode some things with varLen
  while( !input.eof() ) {
    numTerms++;

    string term = input.getString();
    u32 numDocs = input.get32();

    termOff.putString(term);
    termOff.put32(output.currentOffset());

    output.putVarLen(numDocs);
    numDocs2 += numDocs;

    // diff encode
    DocID lastID = 0;
    for(u32 d=0; d<numDocs; d++) {
      DocID curID = input.get32();

      // put diff of DocID across
      output.putVarLen(curID - lastID);
      lastID = curID;

      // get count
      u32 posCount = input.get32();
      output.putVarLen(posCount);

      numHits2 += posCount;

      // diff encode
      u32 lastPos = 0;
      for(u32 p=0; p<posCount; p++) {
        numHits++;
        u32 curPos = input.get32();
        assert(curPos > lastPos || (lastPos == 0 && curPos == 0));
        output.putVarLen(curPos - lastPos);
        lastPos = curPos;
      }
    }
  }

  show(numTerms);
  show(numHits);
  show(numDocs2);
  show(numHits2);
}


