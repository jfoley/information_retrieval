#include "InvertedList.h"
#include <algorithm>


void InvList::putWord(DocID docId, int offset) {
  if(documents.size() == 0 || documents.back() != docId) {
    documents.push_back(docId);
    
    InvListEntry newEntry;
    newEntry.putWord(offset);
    invLists.push_back(newEntry);
  } else {
    invLists.back().putWord(offset);
  }
#if 0
  // if we find it, fill it out
  auto it = docs.find(docId);
  if(it != docs.end()) {
    (it->second).putWord(offset);
    return;
  }

  // stick it in the map if not
  InvListEntry newEntry;
  newEntry.putWord(offset);
  docs[docId] = newEntry;
  //docs.insert(it, std::pair<DocID, InvListEntry>(docId, newEntry));
//#else
  // most efficient std::map insert:
  auto it = docs.lower_bound(docId);

  // not found
  if(it == docs.end() || it->first != docId) {
    InvListEntry newEntry;
    newEntry.putWord(offset);
    docs.insert(it, std::pair<DocID, InvListEntry>(docId, newEntry));
  } else {
    it->second.putWord(offset);
  }
#endif
}

