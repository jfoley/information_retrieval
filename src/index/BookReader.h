#ifndef _BOOKREADER_H
#define _BOOKREADER_H

#include "IndexData.h"

string bookNameFromPath(const string& input);
void indexBook(const string &path, IndexData &index);

void countWordsInBook(const string &, u32 &uniqueWords, u32 &totalWords);
string findPageInBook(const string &path, string pageNum);


#endif

