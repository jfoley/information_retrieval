#include "XMLEvent.h"
#include "../str++.h"

// if the last character was a literal '&'
static char getXMLEscape(istream& in) {
  char c = in.get();
  if(c != '&') return c;
  
  // build up escape, up to four characters + 1 semicolon
  string escape;
  for(int n=0; n<5; n++) {
    char next = in.get();
    if(next == ';') break;
    escape.push_back(next);
  }

  const vector<string> allowed = { "quot", "apos", "lt", "gt", "amp" };
  const char translation[] = { '"', '\'', '<', '>', '&' };
  
  for(size_t i=0; i<allowed.size(); i++) {
    if(allowed[i] == escape) {
      //cout << "Found &"<<escape<<"; -> " << translation[i] << "\n";
      return translation[i];
    }
  }
  
  cerr << "Warning: unidentified escape found: <<&" << escape << ">>\n";
  // ignore errors
  return ' ';
}

static XMLEvent parseXMLEvent(const string &insides) {
  assert(insides.front() != '<' && insides.back() != '>');

  XMLEvent::Type type = XMLEvent::ERROR;

  // classify type
  if(insides.front() == '?' && insides.back() == '?') {
    type = XMLEvent::VERSION_TAG;
  } else if(insides.back() == '/') {
    type = XMLEvent::INDEPENDENT_TAG;
  } else if(insides.front() == '/') {
    type = XMLEvent::END_TAG;
  } else if(insides.size()) {
    type = XMLEvent::START_TAG;
  }

  return XMLEvent(type, insides);
}

XMLEvent getNextEvent(istream &input) {

  string buffer;

  while(input.good()) {
    int c = input.peek();

    if ( c == EOF )
      break;

    // try and commit existing buffer, before starting this tag
    if(c == '<') {
      if(!justWhitespace(buffer)) {
        return XMLEvent(XMLEvent::TEXT, buffer);
      }
      buffer = "";
      input.get();
      continue;
    }
    if(c == '>') {
      input.get();
      return parseXMLEvent(buffer);
    }

    buffer += getXMLEscape(input);
  }

  return XMLEvent(XMLEvent::XML_EOF, "EOF");
}



XMLEvent::XMLEvent(Type t, const string &insides) {
  type = t;
  data = insides;
  // grab up until the first whitespace
  name = insides.substr(0, insides.find(' '));
  if(name[0] == '/') name = name.substr(1);
}

ostream& operator<<(ostream& out, const XMLEvent &me) {
  if(me.type == XMLEvent::ERROR) {
    out << "XMLEvent ERROR: \"" << me.data << "\"";
  }
  else if(me.type == XMLEvent::TEXT) {
    out << "Text: \"" << me.data << "\"";
  } else {
    out << "<" << me.data << ">";
  }
  return out;
}

