#include "../util.h"
#include "../str++.h"

static string nextAttrToken(istream &input) {
  string token = "";
  bool inString = false;
  
  while(input.good()) {
    char c = input.peek();

    // consume everything if in a string until ending quote
    if(inString) {
      if(c == '"') {
        input.get();
        return token;
      }
      token += input.get();
      continue;
    }
    
    // see if we need to enter string state
    if(c == '"') {
      if(!justWhitespace(token)) {
        return token;
      }
      input.get();
      inString = true;
      continue;
    }

    // otherwise, assume space separated tokens
    if(c <= ' ') {
      if(!justWhitespace(token)) {
        return token;
      }
      input.get();
      continue;
    }

    // otherwise, assume = separated tokens
    if(c == '=') {
      if(!justWhitespace(token)) {
        return token;
      }
      input.get();
      return "=";
    }

    token += input.get();
  }

  return "";
}

static vector<string> tokenizeXMLAttrs(const string &insides) {
  stringstream ss;
  ss << insides;

  vector<string> tokens;

  while(ss.good()) {
    string token = nextAttrToken(ss);
    if(token == "") break;
    tokens.push_back(token);
  }

  return tokens;
}

string _getXMLAttr(const string &insides, const string &key, const string& fallback) {
  stringstream ss;
  ss << insides;

  while(ss.good()) {
    if(nextAttrToken(ss) == key && nextAttrToken(ss) == "=") {
      return nextAttrToken(ss);
    }
  }

  return fallback;
}
string getXMLAttr(const string &insides, const string &key, const string& fallback) {
  const vector<string> attrs = tokenizeXMLAttrs(insides);
  
  for(size_t i=0; i<attrs.size()-2; i++) {
    if(attrs[i] == key && attrs[i+1] == "=") {
      return attrs[i+2];
    }
  }

  return fallback;
}

