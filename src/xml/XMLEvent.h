#ifndef _XMLEVENT_H
#define _XMLEVENT_H

#include "../util.h"

// public API:
class XMLEvent;

// read an XMLEvent object from an input stream
XMLEvent getNextEvent(istream &input);

class XMLEvent {
  public:
    enum Type {
      ERROR,
      TEXT,
      VERSION_TAG,
      START_TAG,
      END_TAG,
      INDEPENDENT_TAG,
      XML_EOF,
    };

    XMLEvent(Type t, const string &insides);
    friend ostream& operator<<(ostream& out, const XMLEvent& me);
    
    Type type;
    string name;
    string data;
  private:
};

#endif

