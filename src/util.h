#ifndef _UTIL_H
#define _UTIL_H

#include "types.h"
#include <cassert>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
using namespace std;

#define show(var) \
  cout << __FILE__ << ":" << __LINE__ << ": " << #var << " = " << var << "\n"

template <class T>
string $(const T& obj) {
  stringstream ss;
  ss << obj;
  return ss.str();
}


#endif

