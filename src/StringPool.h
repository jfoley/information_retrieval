#ifndef _STRINGPOOL_H
#define _STRINGPOOL_H

#include "util.h"

// collect a lot of strings using a minimal memory footprint
// modeled after strdata from the ELF format
class StringPool {
  public:
    // insert a string into the pool
    u32 put(const string& str);
    size_t size() const { return offsets.size(); }
    
    // a call to put may invalidate pointers returned from this
    const char* cstr(u32 index) const;
    const char* operator[](u32 index) const { return cstr(index); }

    void clear() {
      data.resize(0);
      offsets.resize(0);
    }

    set<string> keys() const {
      set<string> x;
      for(u32 i=0; i<size(); i++) { x.insert(cstr(i)); }
      return x;
    }

    friend ostream& operator<<(ostream &out, const StringPool &self) {
      out << self[0];
      for(u32 i=1; i<self.size(); i++) {
        out << ", " << self[i];
      }
      return out;
    }
    vector<char> data;
  protected:
    vector<u32> offsets;
  private:
};


#endif

