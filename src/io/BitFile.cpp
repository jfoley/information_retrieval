#include "BitFile.h"

// bitwise/integer log base 2 of n
static u32 logb2(u32 n) {
  u32 p = 0;
  u32 x = 1;

  while(x < n) {
    x *= 2;
    p++;
  }

  if(n == x)
    return p;

  return p-1;
}

BitFile::BitFile(string p, Mode m) : File(p, m) {
  bitsBuffered = 0;
}

BitFile::~BitFile() {
  if(mode == WRITE && valid()) {
    flush();
  }
}

u32 BitFile::currentOffset() const {
  assert(bitsBuffered == 0);
  return File::currentOffset();
}

void BitFile::put1(u32 value) {
  assert(mode == WRITE);

  //cout << "put1:"<<value<<"\n";

  if(bitsBuffered == 8) {
    flush();
  }

  // make sure we have a 1 bit value
  u32 bit = value ? 1 : 0;
  
  // fill in bits from left to right; low to high
  bitBuffer |= bit << bitsBuffered;
  bitsBuffered++;
}

void BitFile::flush() {
  if(mode == WRITE) {
    if(bitsBuffered) {
      put8(bitBuffer);
      bitsBuffered = 0;
      bitBuffer = 0;
    }
  } else {
    bitsBuffered = 0;
  }
}

u32 BitFile::get1() {
  assert(mode == READ);

  if(!bitsBuffered) {
    bitBuffer = get8();
    bitsBuffered = 8;
  }

  // read bits from left to write ; low to high
  u8 bitSet = bitBuffer & 1;
  bitBuffer >>= 1;
  bitsBuffered -= 1;

  return bitSet;
}

// put n 1s followed by a 0
void BitFile::putUnary(u32 n) {
  for(u32 i=0; i<n; i++) {
    put1(1);
  }
  put1(0);
}

// read 1s until a 0 is found
// return count
u32 BitFile::getUnary() {
  u32 n=0;

  while(get1()) {
    n++;
  }

  return n;
}

// put the lowest numBits of x
void BitFile::putNum(u32 x, u32 numBits) {
  for(u32 i=0; i<numBits; i++) {
    u32 pow = numBits-i-1;
    put1(x & (1 << pow));
  }
}

u32 BitFile::getNum(u32 numBits) {
  //cout << "\ngetNum: ";
  u32 x = 0;
  for(u32 i=0; i<numBits; i++) {
    u32 bit = get1();
    //cout << bit;
    x <<= 1;
    x |= bit;
  }
  //cout << "\n";
  return x;
}

void BitFile::putGamma(u32 n) {
  assert(n < 0x80000000); //assert positive
  assert(n != 0);
  u32 len = logb2(n);
  u32 rem = n - (1<<len);

  putUnary(len);
  putNum(rem, len);
}

u32 BitFile::getGamma() {
  u32 len = getUnary();
  u32 rem = getNum(len);
  return (1<<len) + rem;
}

// encode the length part as a gamma code; shifted to avoid zero
void BitFile::putEDelta(u32 n) {
  assert(n != 0);
  u32 len = logb2(n);
  u32 rem = n - (1<<len);

  putGamma(len+1);
  putNum(rem, len);
}

// parse the length part as a gamma code; shifted to avoid zero
u32 BitFile::getEDelta() {
  u32 len = getGamma()-1;
  u32 rem = getNum(len);
  return (1<<len) + rem;
}




