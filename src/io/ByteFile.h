#ifndef _BYTEFILE_H
#define _BYTEFILE_H

#include "File.h"

class ByteFile : public File {
  public:
    ByteFile(string p, Mode m) : File(p, m) { }

    void put32(u32 value);
    u32 get32();
    u32 peek32();

    void putVarLen(u32 value);
    u32 getVarLen();

    void putString(const string& value);
    string getString();
    string peekString();
};

#endif

