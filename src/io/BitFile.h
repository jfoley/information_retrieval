#ifndef _BITFILE_H
#define _BITFILE_H

#include "File.h"

class BitFile : public File {
  public:
    BitFile(string p, Mode m);
    ~BitFile();
    
    u32 currentOffset() const;

    // primitive set/get 1 bit at a time
    // since we need to operate on bytes, a flush is provided
    void put1(u32 value);
    u32 get1();
    void flush();

    // unary ops - 1s then 0
    void putUnary(u32 n);
    u32 getUnary();

    // put/get the lowest numBits of x
    void putNum(u32 x, u32 numBits);
    u32 getNum(u32 numBits);

    // Elias' Gamma Coding
    void putGamma(u32 n);
    u32 getGamma();

    // Elias' Delta Coding
    void putEDelta(u32 n);
    u32 getEDelta();
  private:
    u8 bitBuffer;
    u8 bitsBuffered;
};

#endif

