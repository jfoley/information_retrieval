#include "ByteFile.h"

void ByteFile::put32(u32 value) {
  write(&value, sizeof(u32));
}

u32 ByteFile::get32() {
  u32 value;
  read(&value, sizeof(u32));
  return value;
}

u32 ByteFile::peek32() {
  u32 offset = currentOffset();
  u32 val = get32();
  seek(offset);
  return val;
}

void ByteFile::putVarLen(u32 value) {
  u32 x = value;

  // while there is more data besides these seven bits
  while(x & ~0x7f) {
    put8(0x80 | (x & 0x7f));
    x >>= 7;
  }
  // last byte will have a highest bit of zero
  put8(x & 0x7f);
}

u32 ByteFile::getVarLen() {
  u32 x = 0;
  u32 i = 0;
  u8 nextByte;

  // get at least one byte at a time and pull out (7 bits of good data)
  do {
    nextByte = get8();
    x |= (nextByte & 0x7f) << (i*7);
    i++;
  } while(nextByte & 0x80);

  return x;
}

void ByteFile::putString(const string& value) {
  for(char c : value) {
    put8(c);
  }
  put8(0);
}

string ByteFile::getString() {
  string result;

  while(!eof()) {
    char c = get8();
    if(c == 0)
      break;
    result += c;
  }

  return result;
}

string ByteFile::peekString() {
  u32 offset = currentOffset();
  string str = getString();
  seek(offset);
  return str;
}


