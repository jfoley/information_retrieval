#include "File.h"

File::File(string path, Mode m) {
  mode = m;
  const char* modeStr = (mode == READ) ? "rb" : "wb";
  fp = fopen(path.c_str(), modeStr);
}

File::~File() {
  if(valid()) {
    fclose(fp);
  }
}

u32 File::currentOffset() const {
  assert(valid());
  return ftell(fp);
}

bool File::valid() const {
  return fp != nullptr;
}

// write primitive
void File::write(const void *data, u32 size) {
  assert(mode == WRITE);
  assert(valid());
  fwrite(data, size, 1, fp);
}

// read primitive
void File::read(void *data, u32 size) {
  assert(mode == READ);
  assert(valid());
  assert(!eof());
  
  size_t amt = fread(data, size, 1, fp);
  if(amt != 1) {
    assert(0);
  }
}

void File::put8(u8 value) {
  write(&value, 1);
}

//u8 File::get8() {
//  u8 value;
//  read(&value, 1);
//  return value;
//}


