#ifndef _FILE_H
#define _FILE_H

#include "../util.h"
#include <cstdio>

class File {
  public:
    enum Mode {
      READ,
      WRITE
    };

    File(string path, Mode m);
    ~File();

    bool eof() const {
      // if we've read off the end
      if(feof(fp)) return true;

      // peek ahead if possible
      int next = fgetc(fp);
      if(next == EOF) return true;

      // put it back if we were wrong
      ungetc(next, fp);
      
      return false;
    }
    
    void seek(u32 offset) { fseek(fp, offset, SEEK_SET); }
    u32 currentOffset() const;
    bool valid() const;
    
    // byte-wise access
    void put8(u8 value);
    u8 get8() {
      u8 value;
      assert(!eof());
      read(&value, 1);
      return value;
    }
    void peek8();

  protected:
    void write(const void *data, u32 size);
    void read(void *data, u32 size);
    
    // members
    FILE *fp;
    Mode mode;
};





#endif

