#include "util.h"
#include "TimeValue.h"
#include "str++.h"
#include "QueryParser.h"
#include "SearchIndex.h"


static void usage() { 
  cerr << "usage: ./vocab collection-name\n";
}

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cerr << "Invalid number of arguments!\n";
    usage();
    return -1;
  }

  string collectionName(argv[1]);

  cout << "Creating Vocab for " << collectionName << "\n";

  const string dataFolder = "data/out-"+collectionName+"/";
  const string offsetFile = dataFolder + "termoff.bin";
  const string invListFile = dataFolder + "out.bin";

  u32 numHits = 0; // total number of words in collection

  ByteFile fpOff(offsetFile, File::READ);
  ByteFile fpList(invListFile, File::READ);
  ByteFile fpVocab(dataFolder + "vocab.bin", File::WRITE);

  u32 numTerms = 0;
  u32 numDocs = 0;

  while(!fpOff.eof()) {
    const string term = fpOff.getString();
    const u32 offset = fpOff.get32();

    numTerms++;
    //show(term);

    InvListIter it(offset, &fpList);
    assert(it.valid());

    assert(it.numDocs != 0);
    numDocs += it.numDocs;
    
    u32 hitsPerWord = 0;
    
    while(it.validDocument()) {
      hitsPerWord += it.hitsInDoc();
      it.nextDocument();
    }

    fpVocab.put32(hitsPerWord);
    numHits += hitsPerWord;
  }

  show(numTerms);
  show(numHits);
  show(numDocs);

  ByteFile fpWords(dataFolder + "numWords.bin", File::WRITE);
  fpWords.put32(numHits);

  return 0;
}

