#include "io/ByteFile.h"
#include "io/BitFile.h"
#include "StringPool.h"
#include "TestSystem.h"

static const char* testFileName = "/tmp/test.bin";

TEST(byteFile) {
  const u32 NumChecks = 100;
  {
    ByteFile bof(testFileName, File::WRITE);
    for(u32 i=0; i<NumChecks; i++) {
      bof.put8(i);
      bof.put32(i*21);
      bof.putVarLen(i*17);
    }
    bof.putString("Hello World!");
  }
  {
    ByteFile bif(testFileName, File::READ);
    for(u32 i=0; i<NumChecks; i++) {
      CHECK(bif.get8()==i);
      CHECK(bif.get32() == i*21);
      CHECK(bif.getVarLen() == i*17);
    }
    CHECK(bif.getString() == "Hello World!");
    CHECK(bif.eof());
  }
}

TEST(bitFile) {
  {
    BitFile bf(testFileName, File::WRITE);
    bf.put1(0); // 6
    bf.put1(0); // 5
    bf.put1(1); // 4
    bf.put1(0); // 3
    bf.put1(0); // 2
    bf.put1(0); // 1
    bf.put1(1); // 0
  }
  {
    BitFile bif(testFileName, File::READ);
    CHECK(bif.get1() == 0); // 6
    CHECK(bif.get1() == 0); // 5
    CHECK(bif.get1() == 1); // 4
    CHECK(bif.get1() == 0); // 3
    CHECK(bif.get1() == 0); // 2
    CHECK(bif.get1() == 0); // 1
    CHECK(bif.get1() == 1); // 0
  }
}

TEST(deltaTest) {
  const u32 MaxTest = 60;

  {
    BitFile bof(testFileName, File::WRITE);
    bof.putUnary(8);
    bof.putUnary(0);
    bof.putNum(7,4);
    
    for(u32 i=1; i<MaxTest; i++) {
      bof.putEDelta(i);
    }
  }
  {
    BitFile bif(testFileName, File::READ);
    CHECK(bif.getUnary() == 8);
    CHECK(bif.getUnary() == 0);
    CHECK(bif.getNum(4) == 7);
    
    for(u32 i=1; i<MaxTest; i++) {
      CHECK(bif.getEDelta() == i);
    }
  }
}

TEST(StringPool) {
  StringPool pool;

  string s1 = "Hello World";
  string s2 = "C++";
  string s3 = "Information";
  string s4 = "Retrieval";
  
  pool.put(s1);
  pool.put(s2);
  pool.put(s3);
  pool.put(s4);

  CHECK(pool[0] == s1);
  CHECK(pool[1] == s2);
  CHECK(pool[2] == s3);
  CHECK(pool[3] == s4);
}

#include "ConstStringMap.h"

TEST(ConstStringMap) {
  ConstStringMap<u32> strmap;

  strmap.put("a", 1);
  strmap.put("b", 2);
  strmap.put("c", 3);
  strmap.put("d", 4);

  CHECK(strmap.find("a") == 0);
  CHECK(strmap.find("b") == 1);
  CHECK(strmap.find("c") == 2);
  CHECK(strmap.find("d") == 3);
  CHECK(strmap.find("e") == -1);
  
  CHECK(strmap.get("a", 999) == 1);
  CHECK(strmap.get("b", 999) == 2);
  CHECK(strmap.get("c", 999) == 3);
  CHECK(strmap.get("d", 999) == 4);
  CHECK(strmap.get("e", 999) == 999);

  strmap.put("e",5);
  CHECK(strmap.get("e",0) == 5);
}

#include "RankedList.h"

TEST(RankedList) {
  RankedList rl(10);
  StringPool strpool;

  // prevent StringPool backing store from being resized/moved
  strpool.data.reserve(9*100);

  for(u32 i=0; i<100; i++) {
    strpool.put(stringf("rlt%d",i));

    QueryResult r;
    r.pageID = strpool[i];
    r.score = i;

    rl.insert(r);
  }

  CHECK(rl[0].score == 99);
  CHECK(rl[1].score == 98);
  CHECK(rl[2].score == 97);
  CHECK(rl[3].score == 96);
  CHECK(rl[4].score == 95);
}


