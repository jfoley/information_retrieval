#include "util.h"
#include "TimeValue.h"
#include "str++.h"
#include "PathConfig.h"
#include "index/BookReader.h"
#include "Parser.h"
#include "StringPool.h"

static void usage() { 
  cerr << "usage: ./create_index collection-name\n"
       << "  build collection lists with gen_lists.sh first.\n";
}

#define TimerPoint(name) \
  end.Now(); \
  name = end - start; \
  start.Now()

int main(int argc, char *argv[]) {
  TimeValue start, end;
  TimeValue parse, merge, docs;
  TimeValue final;

  if(argc != 2) {
    cerr << "Invalid number of arguments!\n";
    usage();
    return -1;
  }

  PathConfig config(argv[1]);

  cout << "Processing <" << config.collectionName << ">\n";

  vector<string> paths = config.getBookPaths();
  if(paths.size() == 0) {
    cerr << "No collection in file " << config.inputFile() << "!\n";
    usage();
    return -1;
  }
  
  IndexData index(config.outFolder);

  int i=0;
  for(const string &path : config.getBookPaths()) {
    indexBook(path, index);

    cout << "Document #" << i+1 << " "
         << "Page #" << index.docCount()+1
         << "\n";

    i++;
  }

  index.saveDocuments();

  TimerPoint(parse);
#if 0
  
  // merge partials
  index.collectPartials();

  TimerPoint(merge);

  index.generateFinal();
  TimerPoint(final);
#endif

  cerr << "\n===\n";
  cerr << "Times:\n";
  cerr << " Parse/Indexing: " << stringf("%.4fs\n", parse.Seconds());
#if 0
  cerr << " Merging Indexes:" << stringf("%.4fs\n", merge.Seconds());
  cerr << " Build Final:    " << stringf("%.4fs\n", final.Seconds());
#endif
  return 0;
}

