#ifndef _CONSTSTRINGMAP_H
#define _CONSTSTRINGMAP_H

#include "StringPool.h"
#include <algorithm>

// collect a lot of strings ; like StringPool except has values and implements a binary search; assumes strings are put in order
template <typename V>
class ConstStringMap : public StringPool {
  public:
    u32 put(const string &str, V x) {
      u32 index = StringPool::put(str);
      values.push_back(x);
      assert(size() == index+1);
      assert(values.size() == index+1);
      return index;
    }

    i32 find(const string &key) const {
      u32 left = 0;
      u32 right = size()-1;

      // iterative binary search
      while(1) {
        const u32 middle = (right + left) / 2;
        const char* midstr = cstr(middle);

        if(left == middle || middle == right) {
          return (key == midstr) ? middle : -1;
        }

        // right in the middle?
        if(key == midstr) {
          return middle;
        }
        
        // if there's no more to search, we're done
        if(left == right) {
          return -1;
        }

        // go down the sides of the tree
        if(key < midstr) {
          right = middle;
          continue;
        }
        if(key > midstr) {
          left = middle+1;
          continue;
        }
      }
    }
    
    V operator[](const u32 index) const { return values[index]; }

    V get(const string &key, V fallback) const {
      i32 index = find(key);
      if(index == -1) return fallback;
      return values[index];
    }

  private:
    vector<V> values;

};



#endif

