#include "Parser.h"

// string manip
#include "str++.h"

// stemming
#include "ext/porter.h"

string removePunctuation(const string &line) {
  string result;
  result.reserve(line.size()/2);

  bool ampersand = false;

  for( char c : line ) {
    // skip any squeeze characters
    if(c == '-' || c == '\'')
      continue;

    if(c == '&') {
      ampersand = true;
      continue;
    }

    if (isdigit(c) || isalpha(c)) {
      if(!ampersand) {
        result += tolower(c);
      }
      continue;
    }
    // convert any multiple spaces or punctuation to a single space
    else if(result.size() && result.back() != ' ') {
      ampersand = false;
      result += ' ';
      continue;
    }
  }

  return result;
}

string createStem(const string &word) {
  int newLength = stem((char*)word.c_str(), 0, word.size());
  return word.substr(0, newLength);
}

string cleanWord(const string &word) {
  return createStem(removePunctuation(word));
}


