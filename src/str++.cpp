#include "str++.h"
#include <cstdarg>

string toLower(const string &input) {
  string result;
  result.reserve(input.size());
  
  for(const char c: input) {
    result.push_back(tolower(c));
  }

  return result;
}

bool justWhitespace(const string& input) {
  for(char c : input) {
    if(c > ' ') return false;
  }
  return true;
}

string stringf(const char* fmt, ...) {
	string buf;
	int len = 0;
	va_list args;

  // calculate length
	va_start(args, fmt);
	len = vsnprintf(nullptr, 0, fmt, args)+1;
	va_end(args);

  // resize buf appropriately
	buf.resize(len);

  // fill in data
	va_start(args, fmt);
	len = vsnprintf(&buf[0], buf.size(), fmt, args);
	va_end(args);

  // chop null
	buf.resize(len); 

	return buf;
}

